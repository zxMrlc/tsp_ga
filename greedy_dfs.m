function y = greedy_dfs( u, k, l, n, mp,inq)
% u当前起点，k 步数 ，l 当前代价 ，n 点个数 ， mp 为地图距离（代价）矩阵 ，inq记录已经访问过的点
%这是一个递归函数 matlab默认递归次数只能500 故只能模拟小规模数据集合
%贪婪搜索得到的答案是近似解，是人们生活中常用的解决方案。
if k==n 
y =  l+mp(u,1);
return;%不return 会继续递归 栈溢出
end
p=1;%初始化 p 无意义 
minlen=1e9;
for i = 1:n
    if inq(i)==0&&minlen>mp(u,i)%取与所有点的连边中最小的边 贪婪策略
        minlen=mp(u,i);
        p=i;
    end
end
inq(p)=1;
y = greedy_dfs(p,k+1,l+minlen, n, mp,inq);