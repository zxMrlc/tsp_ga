function y = dp(mp,n)
%mp
%n
INF = 100000;%一个预设的阈值
for i = 1:n
mp(i,i) = INF;%自己不可到达自己 不然在状态转移的过程中 会导致 结果被自环代替
end
dp=zeros(2^(n+1),n+1);
hash = @(x)x+1;
for i = 0:n-1
	dp(hash(i),hash(0)) = mp(hash(i),hash(0));
	%disp(dp(hash(i),hash(0)));
end 
stNum=2^(n-1);
for S=0:stNum-1;
	for i=2:n
		tmp = 2^(i-2);
		if bitand(tmp,S) ~=0
			if S==tmp
				dp(hash(S),i) = mp(1,i);
			else
				dp(hash(S),i) = INF;
				for k=2:n
					if bitand(S,2^(k-2))~=0 && k~=i
					dp(hash(S),i) = min(dp(hash(S),i),dp(hash(S-2^(i-2)),k)+mp(k,i));
					end
				end
			end

		end
	end
end
ans = INF;
for i=1:n
ans = min(ans, dp(stNum,i)+mp(1,i));
end
disp('Exact solution of dynamic programming = ')
disp(ans);
